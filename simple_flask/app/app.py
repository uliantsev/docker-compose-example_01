#!/usr/bin/env python
import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
# DATABASE_URL=
POSTGRES_USER = os.getenv('POSTGRES_USER', '')
POSTGRES_DB = os.getenv('POSTGRES_DB', '')
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', '')
POSTGRES_HOST = os.getenv('POSTGRES_HOST', 'localhost')
app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:5432/{POSTGRES_DB}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class UserModel(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())

@app.route('/')
def home():
    name = os.getenv('ADMINAME', 'Def3')
    # breakpoint()
    result = db.session.execute('SELECT 22 + 2;')
    value = list(result)[0][0]
    return f'Hello, {name}! \n Test value: {value}'


@app.route('/heartbeat')
def heartbeat():
    return f'OK!'


if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
